/* Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <star/scad.h>

#include "data.h"

#define ERR(Expr) if((res = (Expr)) != RES_OK) goto error; else (void)0

static res_T
build_floor(struct scad_geometry** floor)
{
  res_T res = RES_OK;
  double pos[3];
  double d[3];

  pos[0] = - LENGTH / 2.;     
  pos[1] = - WIDTH / 2.;     
  pos[2] = 0;
  d[0] = LENGTH;
  d[1] = WIDTH;
  d[2] = FLOOR_THICKNESS;
  ERR(scad_add_box(NULL, pos, d, floor));
 
exit:
  return res;
error:
  goto exit;
}

static res_T
build_roof(struct scad_geometry** roof)
{
  res_T res = RES_OK;
  double pos[3];
  double d[3];

  pos[0] = - LENGTH / 2.;     
  pos[1] = - WIDTH / 2.;     
  pos[2] = FLOOR_THICKNESS + HEIGHT;
  d[0] = LENGTH;
  d[1] = WIDTH;
  d[2] = FLOOR_THICKNESS;
  ERR(scad_add_box(NULL, pos, d, roof));
 
exit:
  return res;
error:
  goto exit;
}

static res_T
build_wall(struct scad_geometry** wall)
{
  res_T res = RES_OK;
  double pos[3];
  double d[3];
  struct scad_geometry* external = NULL;
  struct scad_geometry* internal = NULL;

  pos[0] = - LENGTH / 2.;     
  pos[1] = - WIDTH / 2.;     
  pos[2] = FLOOR_THICKNESS;
  d[0] = LENGTH;
  d[1] = WIDTH;
  d[2] = HEIGHT;
  ERR(scad_add_box(NULL, pos, d, &external));

  pos[0] = - LENGTH / 2. + WALL_THICKNESS;     
  pos[1] = - WIDTH / 2. + WALL_THICKNESS;     
  pos[2] = FLOOR_THICKNESS;
  d[0] = LENGTH - 2.*WALL_THICKNESS;
  d[1] = WIDTH - 2.*WALL_THICKNESS;
  d[2] = HEIGHT;
  ERR(scad_add_box(NULL, pos, d, &internal));
 
  ERR(scad_cut_geometries(NULL, &external, 1, &internal, 1, wall));

exit:
  if (internal) scad_geometry_delete(internal);
  if (external) scad_geometry_delete(external);
  return res;
error:
  goto exit;
}

static res_T 
build_cavity(struct scad_geometry** cavity)
{
  res_T res = RES_OK;
  double pos[3];
  double d[3];

  pos[0] = - LENGTH / 2. + WALL_THICKNESS;     
  pos[1] = - WIDTH / 2. + WALL_THICKNESS;     
  pos[2] = FLOOR_THICKNESS;
  d[0] = LENGTH - 2.*WALL_THICKNESS;
  d[1] = WIDTH - 2.*WALL_THICKNESS;
  d[2] = HEIGHT;
  ERR(scad_add_box(NULL, pos, d, cavity));

exit:
  return res;
error:
  goto exit;
}

static res_T 
build_ground(struct scad_geometry** ground)
{
  res_T res = RES_OK;
  double pos[3];
  double d[3];

  pos[0] = - 10.*LENGTH / 2.;     
  pos[1] = - 10.*WIDTH / 2.;     
  pos[2] = 0;
  d[0] = 10*LENGTH;
  d[1] = 10*WIDTH;
  d[2] = -20*FLOOR_THICKNESS;
  ERR(scad_add_box(NULL, pos, d, ground));
 
exit:
  return res;
error:
  goto exit;
}


int
main(int argc, char* argv[])
{
  res_T res = RES_OK;
  struct scad_geometry* floor = NULL;  
  struct scad_geometry* roof = NULL;  
  struct scad_geometry* wall = NULL;  
  struct scad_geometry* cavity = NULL;  
  struct scad_geometry* ground =  NULL;  
  struct scad_geometry* list[5];  
  struct scad_geometry* house[4];  
  struct scad_geometry* B_house =  NULL;  
  struct scad_geometry* B_ground =  NULL;  
  struct scad_geometry* C_cavity_wall =  NULL;  
  struct scad_geometry* C_cavity_floor =  NULL;  
  struct scad_geometry* C_cavity_roof =  NULL;  
  
  (void)argc; (void)argv;
 
  /* init scad device */
  ERR(scad_initialize(NULL, NULL, 3));

  /* creation of the 3D elements */
  ERR(build_floor(&floor));
  ERR(build_wall(&wall));
  ERR(build_roof(&roof));
  ERR(build_cavity(&cavity));
  ERR(build_ground(&ground));

  /* scene partitionning.
   * Partitioning consists in identifying the common faces
   * between the 3D objects. This step is essential to obtain a conformal mesh.*/
  ERR(scad_scene_partition());

  /* creation of the boundary faces  */
  list[0] = floor;
  list[1] = roof;
  list[2] = wall;
  list[3] = cavity;
  list[4] = ground;

  house[0] = floor;
  house[1] = roof;
  house[2] = wall;
  house[3] = cavity;

  ERR(scad_geometries_common_boundaries(NULL, list, 5, house, 4, &B_house));
  ERR(scad_geometries_common_boundaries(NULL, list, 5, &ground, 1, &B_ground));

  /* creation of the connections solid/fluid */   
  ERR(scad_geometries_common_boundaries(NULL, &cavity, 1, &wall, 1, &C_cavity_wall));
  ERR(scad_geometries_common_boundaries(NULL, &cavity, 1, &floor, 1, &C_cavity_floor));
  ERR(scad_geometries_common_boundaries(NULL, &cavity, 1, &roof, 1, &C_cavity_roof));

  /* meshing */
  ERR(scad_scene_mesh()); 

  /* stl export */
  ERR(scad_stl_export(floor, "S_floor", 0, 0));
  ERR(scad_stl_export(wall, "S_wall", 0, 0));
  ERR(scad_stl_export(roof, "S_roof", 0, 0));
  ERR(scad_stl_export(cavity, "F_cavity", 0, 0));
  ERR(scad_stl_export(ground, "S_ground", 0, 0));
  ERR(scad_stl_export(B_house, "B_house", 0, 0));
  ERR(scad_stl_export_split(B_ground, "B_ground", 0));
  ERR(scad_stl_export(C_cavity_wall, "C_cavity_wall", 0, 0));
  ERR(scad_stl_export(C_cavity_floor, "C_cavity_floor", 0, 0));
  ERR(scad_stl_export(C_cavity_roof, "C_cavity_roof", 0, 0));

exit:
  if (floor) scad_geometry_delete(floor);
  if (roof) scad_geometry_delete(roof);
  if (wall) scad_geometry_delete(wall);
  if (cavity) scad_geometry_delete(cavity);
  if (ground) scad_geometry_delete(ground);
  if (B_house) scad_geometry_delete(B_house);
  if (B_ground) scad_geometry_delete(B_ground);
  if (C_cavity_wall) scad_geometry_delete(C_cavity_wall);
  if (C_cavity_floor) scad_geometry_delete(C_cavity_floor);
  if (C_cavity_roof) scad_geometry_delete(C_cavity_roof);
  scad_finalize();
  return res;
error:
  goto exit;
}
