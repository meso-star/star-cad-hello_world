# Star-CAD: Hello World

star-cad-hello_world is an example of a program for creating geometric content
based on the Star-CAD library. It allows the creation of a simple house on
a ground. The house is composed of 4 walls, a floor and the roof. The
geometrical parameters are to be filled in the file data.h.

As output, the program generates a set of stl files describing: 

- the solids (S_floor.stl, S_roof.stl, S_walls.stl, S_ground); 
- the fluid cavity (F_cavity); 
- the boundary face of the house (B_house.stl); 
- the boundary faces of the ground (B_ground_*.stl); 
- the connections between the internal cavity and the solids (C_cavity_*.stl). 

These stl files constitute a conformal mesh of the scene that can be used as
input data for a program like [Stardis](https://gitlab.com/meso-star/stardis).

## How to build

This program relies on [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/#tab-readme) package to build. It also
depends on the [Star-CAD](https://gitlab.com/meso-star/star-cad/) library.

You can first install Star-CAD and its dependencies via the star-cad branch of
[star-engine](https://gitlab.com/meso-star/star-engine/-/tree/star-cad) by
following these
[instructions](https://gitlab.com/meso-star/star-engine/-/tree/star-cad).

We then assume that the star-engine is installed in the ~/star-engine/
directory. Then you can clone this repository and build the project:

    ~ $ git clone https://gitlab.com/meso-star/star-cad-hello_world.git
    ~ $ mkdir star-cad-hello_world/build && cd star-cad-hello_world/build
    ~/star-cad-hello_world/build $ cmake ../cmake -DCMAKE_PREFIX_PATH=~/star-engine/local 
    ~/star-cad-hello_world/build $ make 
    ~/star-cad-hello_world/build $ source ~/star-engine/local/etc/star-engine.profile
 
And to execute the program:

    ~/star-cad-hello_world/build $ ./hello_word

## License

Copyright (C) 2023 |Meso|Star> (<contact@meso-star.com>). star-cad-hello_world 
is free software released under the GPL v3+ license: GNU GPL version 3 or
later. You are welcome to redistribute it under certain conditions; refer to
the COPYING file for details.
